import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Customer } from '../models/customer.model';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  form: FormGroup
  status: string
  isConnected: boolean
  model: Customer
  constructor(private cd: ChangeDetectorRef ,private es:ElasticsearchService, private formBuilder: FormBuilder) {
    this.isConnected = false
    this.model = {
      id: '',
      fullname: '',
      age: 0,
      address: '',
      published: null
    }
    this.form = this.formBuilder.group(this.model)
   }

  ngOnInit() {
    this.es.isAvailable().then(() => {
        this.status = 'OK'
        this.isConnected= true
    }, error => {
        this.status = 'ERROR'
        this.isConnected = false
        console.log('Server is down!', error)
    }).then(() => {
      this.cd.detectChanges()
    })
    
  }
onSubmit(value) {
  this.es.addToIndex(value)
}


}
