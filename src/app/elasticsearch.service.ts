import { Injectable } from '@angular/core'
import * as elasticsearch from 'elasticsearch'

@Injectable()
export class ElasticsearchService {
  private client = elasticsearch.clien

  queryAlldocs = {
    'query': {
      'match_all': {}
    }, 
    'sort': [
          { '_uid': { 'order': 'asc' } }
    ]
  }

  constructor() {
    if (!this.client) {
      this.connect();
    }
  }
  private connect() {
    this.client = new elasticsearch.Client({
      host: 'localhost:9200',
      user: 'elastic',
      password: 'dadado',
      log: 'trace'
    })
  }

  getInstance() {
    return this.client
  }

  createIndex(name): any {
    this.client.indices.create(name)
    .then(res => {
      console.log(res)
    }, error => {
      console.error(error)
    })
  }

  isAvailable(): any {
    return this.client.ping({
      requestTimeout: Infinity,
       body: 'Bonjour Elastic'
    })
  }
  addToIndex(task) {
    return this.client.create({
      index: 'test',
      type: 'customer',
      id: task.id,
      body: {
        fullname: task.fullname,
        age: task.age,
        adress: task.address,
        published_at:  new Date().toLocaleString()
      }

    }).then(res => {
      console.log(res)
    }, error => {
      console.error(error)
    })
  }

  getAllDocuments(_index, _type) {
   return this.client.search({
      index: _index,
      type: _type,
      body: this.queryAlldocs,
      filter_path: ['hits.hits._source']  
    })
  }


  getAllDocumentsWithScroll(_index, _type, _size) {
    return this.client.search({
       index: _index,
       type: _type,
       scroll: '30s',
       'size': _size,
       body: {
          
          'query': {
            'match_all': {}
          }, 
          'sort': [
            { '_uid': { 'order': 'asc' } }
          ]
        
       }
      
     })
   }

   getNextPage(scroll_id) {
     return this.client.scroll({
      scrollId: scroll_id,
      scroll: '1m',
      filterPath: ['hits']
     })

   }

   fullTextSearch(_index, _type, _field, _queryText) {
      return this.client.search({
        index: _index,
        type: _type,
        body: {
          "query": {
             "match_phrase_prefix": {
               [_field]: _queryText
             }
          }
        },
        _source: ['fullname', 'adress']
      })
   }

}
