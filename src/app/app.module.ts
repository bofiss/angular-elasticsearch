import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component'
import { ElasticsearchService } from './elasticsearch.service'
import { CreateIndexComponent } from './create-index/create-index.component';
import { AddTaskComponent } from './add-task/add-task.component';
import { ShowTaskComponent } from './show-task/show-task.component';
import { ViewTaskComponent } from './view-task/view-task.component';
import { SearchTaskComponent } from './search-task/search-task.component'

 
const routes: Routes = [
    { path: '', redirectTo: 'add', pathMatch: 'full' },
    { path: 'add', component: AddTaskComponent },
    { path: 'search', component: SearchTaskComponent },
    { path: 'customers', component: ShowTaskComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CreateIndexComponent,
    AddTaskComponent,
    ShowTaskComponent,
    ViewTaskComponent,
    SearchTaskComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ElasticsearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
