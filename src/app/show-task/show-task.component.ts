import { Component, OnInit } from '@angular/core';
import { Customer } from '../models/customer.model';
import { ElasticsearchService } from '../elasticsearch.service';

@Component({
  selector: 'app-show-task',
  templateUrl: './show-task.component.html',
  styleUrls: ['./show-task.component.css']
})
export class ShowTaskComponent implements OnInit {

  private static readonly INDEX = 'test'
  private static readonly TYPE = 'customer'
  private static readonly SIZE = 2
  customers: Customer[]
  haveNextPage = false
  scrollId : string 
  notice: string



  constructor(private es:ElasticsearchService) {
    this.scrollId = ''
    this.notice = ''
    this.haveNextPage = false
   }

  ngOnInit() {
    this.es.getAllDocumentsWithScroll(ShowTaskComponent.INDEX, ShowTaskComponent.TYPE, ShowTaskComponent.SIZE).then(res => {
    console.log(res._scroll_id)
      this.customers = res.hits.hits
      if(res.hits.hits.length < res.hits.total) {
       this.haveNextPage = true
       this.scrollId = res._scroll_id
      }
        console.log(res);
      }, error => {
        console.error(error);
      }).then(() => {
        console.log('Show Customer Completed!');
      });
  }

  showNextPage() {
     this.es.getNextPage(this.scrollId).then(res => {
       this.customers = res.hits.hits
       if(!res.hits.hits){
         this.haveNextPage = false
         this.notice = "There no more Customers !"
       }
       console.log(res)
     }, error => {
       console.error(error)
     }).then(() => {
       console.log('Show customer Completed!')
     })
  }


  

}
