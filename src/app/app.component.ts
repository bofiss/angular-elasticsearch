import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from './elasticsearch.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  title = 'app';

  constructor(private es:ElasticsearchService) {}

  ngOnInit(): void {
    this.es.getInstance().ping({
      requestTimeout: Infinity,
       body: 'Bonjour Elastic'
    })
  }


}
