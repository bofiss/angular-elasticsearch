import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { Customer } from '../models/customer.model';

@Component({
  selector: 'app-search-task',
  templateUrl: './search-task.component.html',
  styleUrls: ['./search-task.component.css']
})
export class SearchTaskComponent implements OnInit {
  queryText:string
  lastKeypress = 0
  customers: Customer[]
  
  private static readonly INDEX = 'test'
  private static readonly TYPE = 'customer'
  constructor(private es: ElasticsearchService) { 
    this.queryText = ''
  }

  ngOnInit() {
  }

  search($event) {
    if ($event.timeStamp - this.lastKeypress > 100) {
      this.queryText = $event.target.value;
      this.queryText = $event.target.value
      this.es.fullTextSearch(SearchTaskComponent.INDEX, SearchTaskComponent.TYPE, 'adress', this.queryText)
        .then(res => {
          this.customers = res.hits.hits
        }, error => {
          console.error(error)
        }).then(() => {
          console.log('Search Completed!')
      })
    }
    this.lastKeypress = $event.timeStamp
  }  

}
