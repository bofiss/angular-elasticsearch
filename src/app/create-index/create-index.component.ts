import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms'
import { ElasticsearchService } from '../elasticsearch.service';
@Component({
  selector: 'app-create-index',
  templateUrl: './create-index.component.html',
  styleUrls: ['./create-index.component.css']
})
export class CreateIndexComponent implements OnInit {

  form: FormGroup
  status: string
  isConnected: boolean

  constructor(private cd: ChangeDetectorRef, private formBuilder: FormBuilder, private es: ElasticsearchService) {
    this.isConnected = false
    this.form = formBuilder.group({
      index: ''
    })
   }

  ngOnInit() {
    this.es.isAvailable().then(() => {
        this.status = 'OK'
        this.isConnected= true
    }, error => {
        this.status = 'ERROR'
        this.isConnected = false
        console.log('Server is down!', error)
    }).then(() => {
      this.cd.detectChanges()
    })
  }

  onSubmit(value) {
    this.es.createIndex({index: value.index})
  }

}
