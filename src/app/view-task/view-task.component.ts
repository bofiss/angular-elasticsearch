import { Component, OnInit, Input } from '@angular/core';
import { Customer } from '../models/customer.model';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {
   
  @Input() customer: Customer
  constructor() { }

  ngOnInit() {
  }

}
