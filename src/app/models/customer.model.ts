export interface Customer {
    id: string;
    fullname: string;
    age: number;
    address: string;
    published: string;
}